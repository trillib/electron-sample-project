import "bootstrap/dist/js/bootstrap.bundle.min.js";
import * as $ from "jquery";
import {PersonDatabase} from "../util/Database";
import {ipcRenderer} from "electron";

const pb = new PersonDatabase();

document.addEventListener("DOMContentLoaded", () => {
    initListeners(); // add listers
});

function initListeners() {
    $("#addbutton").click( () => {
        handleAddButtonClick();
    });
}

function handleAddButtonClick() {
    const firstname = $("#firstnameinput").val() as string;
    const lastname = $("#lastnameinput").val() as string;
    pb.saveUser(firstname, lastname, (error, value) => {
        ipcRenderer.send("user:add", value);
    });
}
