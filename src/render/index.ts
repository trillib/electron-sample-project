// main script file to set listers with jQuery and route click events
// this import is this way to get the bundled version of Bootsrap incorporating popper.js
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import * as $ from "jquery";
import {PersonDatabase} from "../util/Database";
import {ipcRenderer} from "electron";

const pb = new PersonDatabase();

document.addEventListener("DOMContentLoaded", () => {
    initListeners(); // add listers
    fillInTable();
});

ipcRenderer.on("user:add", (e, value) => {
    $("#persontable").append("<tr><td>" + value.firstname + "</td><td>" + value.lastname + "</td></tr>");
});

function fillInTable() {
    pb.getUsers((error, value) => {
        for (const person of value) {
            $("#persontable").append("<tr><td>" + person.firstname + "</td><td>" + person.lastname + "</td></tr>");
        }
    });
}

function initListeners() {
    $("#newUserButton").click( () => {
        handleNewUserButtonClick();
    });
}

function handleNewUserButtonClick() {
    ipcRenderer.send("addWindow:open");
}
