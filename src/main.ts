import { app, BrowserWindow, ipcMain} from "electron";
import * as path from "path";
import * as url from "url";

let mainWindow: Electron.BrowserWindow;
let addWindow: Electron.BrowserWindow;

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    height: 1200,
    width: 900,
  });

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, "../ressource/index.html"),
    protocol: "file:",
    slashes: true,
  }));

  // Emitted when the window is closed.
  mainWindow.on("closed", () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

function createAddWindow() {
  addWindow = new BrowserWindow({
    width: 400,
    height: 300,
    title: "Person hinzufügen",
  });
  addWindow.loadURL(url.format({
    pathname: path.join(__dirname, "../ressource/addWindow.html"),
    protocol: "file:",
    slashes: true,
  }));
  // Handle garbage collection
  addWindow.on("close", () => {
    addWindow = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On OS X it"s common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on("addWindow:open", () => {
  createAddWindow();
});

ipcMain.on("user:add", (e, item) => {
  mainWindow.webContents.send("user:add", item);
  addWindow.close();
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
