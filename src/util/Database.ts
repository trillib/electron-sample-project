import Datastore = require("nedb");

export class PersonDatabase {
    private db = new Datastore({filename: "person.db", autoload: true});
    public saveUser(firstname: string, lastname: string, onComplete?: (error: Error, value: any) => void) {
        const person = {firstname, lastname};
        this.db.insert(person, onComplete);
    }

    public getUsers(onComplete: (error: Error, value: Array<{firstname: string, lastname: string}>) => void) {
        this.db.find({}, onComplete);
    }
}
